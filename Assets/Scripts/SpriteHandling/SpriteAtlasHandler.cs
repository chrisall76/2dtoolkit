﻿using UnityEngine;
using System.Collections;

public class SpriteAtlasHandler : MonoBehaviour {

	public Texture2D[] atlasTextures;
	public Rect[] rects;
	public Texture2D atlas;
	
	//Testing Variables ONLY
	private Vector2[] newUV;
	
	// Use this for initialization
	void Awake () {
		atlas = new Texture2D(2048, 2048);
		rects = atlas.PackTextures(atlasTextures, 0, 2048);
		atlas.wrapMode = TextureWrapMode.Clamp;
		atlas.filterMode = FilterMode.Point;
		atlas.Apply();
	}
	
	public void SetUV(int rectNum, bool Mirroed, Mesh mesh, int Direct, Material material){
		newUV = new Vector2[mesh.uv.Length];
		Texture2D atlasTemp = atlas;
		
		Vector2 Start = new Vector2(rects[rectNum].x, rects[rectNum].y);
		Vector2 Size = new Vector2(rects[rectNum].width, rects[rectNum].width);
		
		newUV[0] = Start;
		newUV[1] = Start + Size;
		newUV[2] = Start + Vector2.right * Size.x;
		newUV[3] = Start + Vector2.up * Size.y;
		
		
		atlasTemp.Apply();
		mesh.uv = newUV;
		
		material.mainTexture = atlasTemp;
	}
}
