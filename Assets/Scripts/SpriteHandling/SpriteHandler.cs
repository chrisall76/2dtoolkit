﻿using UnityEngine;
using System.Collections;

public class SpriteHandler : MonoBehaviour {
	
	public Material material;
	public SpriteAtlasHandler atlasHandler;
	public Mesh thisMesh;
	private bool Mirroredz = false;
	
	//Private Vars
	//Animation
	private bool Animating = false;
	private float[] waitTime;
	private int[] animNumberz;
	
	// Use this for initialization
	void Start () {
		atlasHandler.SetUV(0, false, thisMesh, 0, material);
	}
	
	public void changeSprite(bool Mirrored, int animNumber){
		Mirroredz = Mirrored;
		atlasHandler.SetUV(animNumber, Mirrored, thisMesh, 0, material);
	}
	
	public void animateSprite(bool Mirrored, int[] animNumbers, float[] intervals){
		Mirroredz = Mirrored;
		animNumberz = animNumbers;
		waitTime = intervals;
		
		if(Animating == false){
		    StartCoroutine(animate());
		}
	}
	
	IEnumerator animate(){
		Animating = true;
		for(int i = 0; i < animNumberz.Length; i++){
		    atlasHandler.SetUV(animNumberz[i], Mirroredz, thisMesh, 0, material);
	        yield return new WaitForSeconds(waitTime[i]);
			if(i == animNumberz.Length){
				i = 0;
			}
		}
		Animating = false;
	}
}
