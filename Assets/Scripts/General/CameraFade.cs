﻿using UnityEngine;
using System.Collections;

public class CameraFade : MonoBehaviour {
	
	//Public 
    public Texture2D fadeOutTexture;
    public float fadeSpeed= 0.3f;
 
    public int drawDepth= -1000;
 
	//Priivate 
    private float alpha= 1.0f; 
    private int fadeDir= -1;
 
 
    void  Start (){
	    alpha=1;
    }
	
    void  OnGUI(){
 
	    alpha += fadeDir * fadeSpeed * Time.deltaTime;	
	    alpha = Mathf.Clamp01(alpha);	
		
		Color temp = GUI.color;
	    temp.a = alpha;
		GUI.color = temp;
	    GUI.depth = drawDepth;
	    GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
    }
	
    public void  fadeIn (float fSpeed){
		fadeSpeed = fSpeed;
	    fadeDir = -1;	
    }

    public void  fadeOut (){
	    fadeDir = 1;	
    }
}