﻿using UnityEngine;
using System.Collections;

public class FadeIn : MonoBehaviour {
	
	private CameraFade fade;
	public float fadeTime = 5.0f;
	
	// Use this for initialization
	void Start () {
		fade = GameObject.Find("Main").GetComponent<CameraFade>();
		fade.fadeIn(0.15f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
