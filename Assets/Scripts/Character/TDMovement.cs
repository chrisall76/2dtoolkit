﻿using UnityEngine;
using System.Collections;

public class TDMovement : MonoBehaviour {
	
	public SpriteHandler spritehandle;
	public SoundHandler soundHandle;
	
	//Movement
	private float moveSpeed = 2.5f;
	public float walkSpeed = 2.5f;
	public float runSpeed = 5f;
	public float jumpDis = 10f;
	
	//Sound
	private float SInv = 0.5f;
	public float walkSInv = 0.5f;
	public float runSInv = 0.25f;
	
	//Frames
	public int[] Walk = new int[3];
	public float[] WalkInt = new float[3];
	
    //Collision Checking
	private bool isGrounded = false;
	public float raycastDist = 1.15f;
	
	void Start () {
	
	}
	
	void Update () {
		//Movement
		gameObject.transform.Translate(Input.GetAxis("Horizontal") * (moveSpeed * Time.deltaTime),0,0);
		
		if(Input.GetButton("Jump")){
			if(isGrounded == true){
				isGrounded = false;
				Vector3 temp = rigidbody.velocity;
				temp.y = jumpDis;
				rigidbody.velocity = temp;
			}
		}
		
		//Speed Handler
		if(Input.GetButton("Run")){
			SInv = runSInv;
			if(isGrounded == true){
			    moveSpeed = runSpeed;
			}else{
				moveSpeed = runSpeed/2;
			}
		}
		if(Input.GetButton("Run") == false){
			SInv = walkSInv;
			if(isGrounded == true){
			    moveSpeed = walkSpeed;
			}else{
				moveSpeed = walkSpeed/1.5f;
			}
		}
		
		
		//Animation
		if(Input.GetAxis("Horizontal") != 0){
			spritehandle.animateSprite(false, Walk, WalkInt);
			if(isGrounded == true){
				soundHandle.Sound("Wood", SInv);
			}
		}
	}
	
	void LateUpdate(){
		RaycastHit hit;
		Vector3 dir = new Vector3(0,-1,0);
		
		if(Physics.Raycast(transform.position, dir, out hit, raycastDist)){
			isGrounded = true;
		}else{
			isGrounded = false;
		}
	}
}
