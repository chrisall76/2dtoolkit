﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {
	
	private bool selfControl = true;
	public GameObject target;
	public float Speed = 2.5f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(selfControl == true){
			Vector3 temp = transform.position;
			temp.x = Mathf.Lerp(temp.x, target.transform.position.x, Speed * Time.deltaTime);
			transform.position = temp;
		}
	}
}
