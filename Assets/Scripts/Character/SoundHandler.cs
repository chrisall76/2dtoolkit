﻿using UnityEngine;
using System.Collections;

public class SoundHandler : MonoBehaviour {
	
	//private
	private bool sounding = false;
	private string MatType;
	private float inter;
	private int sInt = 0;
	//public
	public AudioClip[] sounds;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Sound(string MaterType, float interval){
		MatType = MaterType;
		inter = interval;
		if(sounding == false){
			sounding = true;
			StartCoroutine(Sounding());
		}
		
		if(MatType == "Wood"){
			sInt = 0;
		}
	}
	
	IEnumerator Sounding(){
		Debug.Log("Reached");
		audio.clip = sounds[sInt];
		audio.Play();
		yield return new WaitForSeconds(inter);
		sounding = false;
	}
}
